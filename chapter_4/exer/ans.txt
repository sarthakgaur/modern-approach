// 3, 5, 6 are wrong
1.
    a. 1 2
    b. 0
    c. 1
    d. 0 

2. In C89, the result of (-i)/j is undefined. The value could be rounded up or down. In C99 the result of (-i)/j and -(i/j) will be the same. Modern CPUs round towards 0.

3.
    a. 1
    b. -1, -2
    c. -1, -2
    d. 1 // This is wrong, should be 1 or 2.

4.
    a. 1
    b. -1
    c. -1
    d. 1

5.
    a. 3
    b. -3, 2
    c. -3, 2
    d. 3 // c, d are wrong

6.
    a. 3
    b. -3
    c. -3
    d. 3 // c, d are wrong

7. Let's take total = 10.
    1. Using old steps:
        total - 1 = 9
        9 % 10 = 9
        9 - 9 = 0
        check digit = 0

    2. Using alternate steps:
        total % 10 = 0
        10 - 0 = 10
        check digit = 10

    Check digits are not the same and a check digit can have only 1 digit.

8. Yes.

9.
    a. 63 8
    b. 3 2 1
    c. 2 -1 3
    d. 0 0 0

10.
    a. 12 12
    b. 3 4
    c. 2 8
    d. 6 9

11.
    a. 0 2
    b. 4 11 6
    c. 0 8 7
    d. 3 4 5 4

12.
    a. 6 16
    b. 6 -7
    c. 6 23
    d. 6 15

13. ++i is exactly like i += 1. i++ returns i, but ++i returns i + 1.

14.
    a. (((a * b) - (c * d)) + e)
    b. (((a / b) % c) / d)
    c. ((((- a) - b) + c) - (+ d))
    d. (((a * (- b)) / c) - d)

15.
    a. i=3, j=2
    b. i=0, j=2
    c. i=1, j=2
    d. i=1, j=3
